var express = require('express');
var cors = require('cors');
var methodOverride = require('method-override');
var ejs = require('ejs');

var bodyParser = require('body-parser');

const googleTrends = require('google-trends-api');
var quiche = require('quiche');
 
var app = express();

app.use(methodOverride());
app.use(cors());
app.set('views', './views');
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var imagesArray;

var keywordsMatrixPosition = 0;

var colors = [
	'1ABC9C',
	'8E44AD',
	'34495E',
	'2C3E50',
	'F1C40F',
	'16A085',
	'2ECC71',
	'27AE60',
	'3498DB',
	'2980B9',
	'9B59B6',
	'F39C12',
	'E67E22',
	'D35400',
	'E74C3C',
	'C0392B',
	'ECF0F1',
	'BDC3C7',
	'95A5A6',
	'7F8C8D'
];

app.post('/get-user', function(req, res) {
});

app.post('/over-time', function(req, res) {
	imagesArray = [];
	getGoogleTrendsOverTime(req, res);
});

app.post('/compare-over-time', function(req, res) {
	imagesArray = [];
	getGoogleTrendsCompareOverTime(req, res);
});

app.post('/by-region', function(req, res) {
	imagesArray = [];
	getGoogleTrendsByRegion(req, res);
});

app.post('/related-queries', function(req, res) {
	imagesArray = [];
	getGoogleRelatedQueries(req, res);
});

function getGoogleTrendsOverTime(req, res) {
	var data = {
		keyword: req.body.keywords[keywordsMatrixPosition],
		startTime: new Date(req.body.timeframe),
		endTime: new Date(Date.now())
	};
	googleTrends.interestOverTime(data).then(
		function(results) {
			// Set condition length
			if(
				(
					JSON.parse(results).default.timelineData.length == 0 &&
					req.body.outputEmptyResults
				) ||
				(
					JSON.parse(results).default.timelineData.length > 0
				)
			) {
				var subset = {
					keyword: data.keyword,
					images: generateLine('Google Trends Over Time - ' + data.keyword, data.keyword, JSON.parse(results).default.timelineData)
				}
				imagesArray.push(subset);
			}

			keywordsMatrixPosition++;
			if(keywordsMatrixPosition < req.body.keywords.length)
				getGoogleTrendsOverTime(req, res);
			else {
				keywordsMatrixPosition = 0;
				res.send({
					passed: true,
					data: imagesArray
				});
			}
		}).catch(function(err) {
			console.error(err);
		}
	);
}

function getGoogleTrendsCompareOverTime(req, res) {
	var data = {
		keyword: req.body.keywords,
		startTime: new Date(req.body.timeframe),
		endTime: new Date(Date.now())
	};
	googleTrends.interestOverTime(data).then(
		function(results) {
			var subset = {
				keyword: data.keyword,
				images: generateMultiLine('Google Trends Over Time - ' + data.keyword, data.keyword, JSON.parse(results).default.timelineData)
			}

			res.send({
				passed: true,
				data: subset
			});
		}).catch(function(err) {
			console.error(err);
		}
	);
}

function getGoogleTrendsByRegion(req, res) {
	var data = {
		keyword: req.body.keywords[keywordsMatrixPosition],
		startTime: new Date(req.body.timeframe),
		endTime: new Date(Date.now()),
		geo: req.body.geo
	};
	googleTrends.interestByRegion(data).then(
		function(results) {
			if(
				(
					JSON.parse(results).default.geoMapData.length == 0 &&
					req.body.outputEmptyResults
				) ||
				(
					JSON.parse(results).default.geoMapData.length > 0
				)
			) {
				var subset = {
					keyword: data.keyword,
					images: generateBar('Google Trends By Region - ' + data.keyword, data.keyword, JSON.parse(results).default.geoMapData)
				}

				imagesArray.push(subset);
			}

			keywordsMatrixPosition++;
			if(keywordsMatrixPosition < req.body.keywords.length)
				getGoogleTrendsByRegion(req, res);
			else {
				keywordsMatrixPosition = 0;
				res.send({
					passed: true,
					data: imagesArray
				});
			}
		}).catch(function(err) {
			console.error(err);
		}
	);
}

function getGoogleRelatedQueries(req, res) {
	var data = {
		keyword: req.body.keywords[keywordsMatrixPosition]
	};
	googleTrends.relatedQueries(data).then(
		function(results) {
			// Generate the graph
			for(var i=0; i<JSON.parse(results).default.rankedList.length; i++) {
				// Set condition length
				if(
					(
						JSON.parse(results).default.rankedList[i].rankedKeyword.length == 0 &&
						req.body.outputEmptyResults
					) ||
					(
						JSON.parse(results).default.rankedList[i].rankedKeyword.length > 0
					)
				) {
					var subset = {
						keyword: data.keyword,
						images: generateBar('Google Trends Related Queries - ' + data.keyword, data.keyword, JSON.parse(results).default.rankedList[i].rankedKeyword)
					}

					imagesArray.push(subset);
				}
			}
			keywordsMatrixPosition++;
			if(keywordsMatrixPosition < req.body.keywords.length)
				getGoogleRelatedQueries(req, res);
			else {
				keywordsMatrixPosition = 0;
				res.send({
					passed: true,
					data: imagesArray
				});
			}
		}).catch(function(err) {
			console.error(err);
		}
	);
}

function generateLine(title, keyword, values) {
	// Build values
	var valuesArray = [];
	for(var i=0;i<values.length;i++) {
		valuesArray.push(values[i].value);
	}
	var chart = new quiche('line');
	chart.setWidth(600);
	chart.setHeight(365);
	chart.setTitle(title);
	chart.addData(valuesArray, keyword, colors[Math.floor(Math.random()*colors.length)]);
	if(values.length > 0) chart.addAxisLabels('x', [dateFormat(values[0].time), dateFormat(values[values.length-1].time)]);
	chart.setAutoScaling();
	chart.setAxisRange('y', 0, 100, 10);
	chart.setLegendBottom();
	chart.setTransparentBackground();

	return chart.getUrl(true);
}

function generateMultiLine(title, keywords, values) {
	// Build values
	var valuesArray = [];
	for(var i=0;i<keywords.length;i++) {
		valuesArray[i] = [];
		for(var j=0;j<values.length;j++) {
			valuesArray[i].push(values[j].value[i]);
		}
	}
	var chart = new quiche('line');
	chart.setWidth(600);
	chart.setHeight(365);
	chart.setTitle(title);
	for(var i=0;i<valuesArray.length;i++) {
		chart.addData(valuesArray[i], keywords[i], colors[Math.floor(Math.random()*colors.length)]);
	}
	if(values.length > 0) chart.addAxisLabels('x', [dateFormat(values[0].time), dateFormat(values[values.length-1].time)]);
	chart.setAutoScaling();
	chart.setAxisRange('y', 0, 100, 10);
	chart.setLegendBottom();
	chart.setTransparentBackground();

	return chart.getUrl(true);
}

function generateBar(title, keyword, values) {
	var chart = new quiche('bar');

	chart.setWidth(600);
	chart.setHeight(365);
	chart.setTitle(title);
	chart.setBarWidth(0);
	chart.setBarSpacing(4);
	chart.setLegendBottom(); // Put legend at bottom
	chart.setTransparentBackground(); // Make background transparent
	for(var i=0;i<values.length;i++) {
		var label = '';
		if(values[i].geoName !== undefined) label = values[i].geoName;
		else if(values[i].query !== undefined) label = values[i].query;
		chart.addData(values[i].value, label, colors[i]);
	}
	chart.setAutoScaling(); // Auto scale y axis

	return chart.getUrl(true);
}

function generatePie(title, keyword, values) {
	// Build values
	var labelsArray = [];
	var valuesArray = [];
	for(var i=0;i<values.length;i++) {
		labelsArray.push(values[i].geoName);
		valuesArray.push(values[i].value);
	}
	var chart = new quiche('pie');

	chart.setWidth(600);
	chart.setHeight(365);
	chart.setTitle(title);
	chart.addData(valuesArray);
	chart.addAxisLabels('x', labelsArray);
	chart.setAutoScaling();
	chart.setTransparentBackground();

	return chart.getUrl(true);
}

function dateFormat(time) {
	var d = new Date(parseInt(time)*1000);
	return d.getFullYear() + '/' + (d.getMonth() + 1) + '/' + d.getUTCDate();
}

app.listen(3000);